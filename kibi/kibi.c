/*
 * Copyright (c) 2010-2011, Benjamin Drung <benjamin.drung@gmail.com>
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

// C standard libraries
#include <locale.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// C POSIX libraries
#include <unistd.h>

#include <config.h>
#include "kibi.h"

#ifdef DEBUG
#define debug(format, ...) printf("libkibi: DEBUG: " format, __VA_ARGS__)
#else
#define debug(format, ...) do{ } while(0)
#endif

#define malloc_error(size) fprintf(stderr, \
                                   "\nlibkibi: malloc(%zu) failed in %s.\n\n", \
                                   size, __func__)

#ifdef __GNUC__
#define likely(x)   __builtin_expect((x),1)
#define unlikely(x) __builtin_expect((x),0)
#else
#define likely(x)   (x)
#define unlikely(x) (x)
#endif

#define BASE2_SIZE_LENGTH 10
#define BASE10_SIZE_LENGTH 9
#define SYSTEM_CONFIG_FILE "/etc/byteprefix"
#define MAX_LEN 20
#define PREFIX_COUNT 7

typedef struct prefix_s {
    char *(*format_size)(const struct prefix_s*, kibi_filesize_t, const char*);
    int (*format_n)(const struct prefix_s*, char*, size_t, kibi_filesize_t,
                    const char*);
    double (*get_factor)(unsigned int level);
    const char *prefix[PREFIX_COUNT];
    const char *size[PREFIX_COUNT];
    const char *transfer_rate[PREFIX_COUNT];
} prefix_t;

static const prefix_t *selected_prefix = NULL;
static const struct lconv *locale = NULL;

static char *format_size_base10(const prefix_t *units, kibi_filesize_t size,
                                const char *suffix) {
    char *formatted_size = NULL;
    kibi_filesize_t rounded_size;
    int i;
    size_t length;

    if(unlikely(size < 1000)) {
        // strlen("999") = 3
        length = 3 + 1 + strlen(suffix) + 1;
        formatted_size = malloc(length);
        if(unlikely(formatted_size == NULL)) {
            malloc_error(length);
            return NULL;
        }
        snprintf(formatted_size, length, "%llu %s", size, suffix);
    } else { // size >= 1000
        i = 1;
        // 999950 will be rounded to 1,000,000
        while(size >= 999950) {
            i++;
            size /= 1000;
        }
        if(size < 9995) {
            rounded_size = (size + 5) / 10;
            // strlen("9.99") = 4
            length = 4 + 1 + strlen(units->prefix[i]) + strlen(suffix) + 1;
            formatted_size = malloc(length);
            if(unlikely(formatted_size == NULL)) {
                malloc_error(length);
                return NULL;
            }
            snprintf(formatted_size, length, "%llu%s%02llu %s%s",
                     rounded_size / 100, locale->decimal_point,
                     rounded_size % 100, units->prefix[i], suffix);
        } else { // size >= 9995 (which will be rounded to 10,000)
            rounded_size = (size + 50) / 100;
            // strlen("999.9") = 5
            length = 5 + 1 + strlen(units->prefix[i]) + strlen(suffix) + 1;
            formatted_size = malloc(length);
            if(unlikely(formatted_size == NULL)) {
                malloc_error(length);
                return NULL;
            }
            snprintf(formatted_size, length, "%llu%s%01llu %s%s",
                     rounded_size / 10, locale->decimal_point,
                     rounded_size % 10, units->prefix[i], suffix);
        }
    }

    return formatted_size;
}

static char *format_size_base2(const prefix_t *units, kibi_filesize_t size,
                               const char *suffix) {
    char *formatted_size = NULL;
    kibi_filesize_t carry_10 = 0;
    kibi_filesize_t carry_100 = 0;
    kibi_filesize_t rounded_size;
    int i;
    size_t length;

    if(size < 1024) {
        // strlen("1023") = 4
        length = 4 + 1 + strlen(suffix) + 1;
        formatted_size = malloc(length);
        if(unlikely(formatted_size == NULL)) {
            malloc_error(length);
            return NULL;
        }
        snprintf(formatted_size, length, "%llu %s", size, suffix);
    } else {
        i = 1;
        // 1023.5 * 1024 = 1,048,064 will be rounded to 1024 * 1024 = 2 ^ 20.
        while(size >= 1048064) {
            i++;
            carry_10 = (carry_10 + 10 * (size & 0x3FF)) >> 10;
            carry_100 = (carry_100 + 100 * (size & 0x3FF)) >> 10;
            size >>= 10;
        }

        rounded_size = (size * 100 + carry_100 + 512) >> 10;
        if(rounded_size < 1000) { // rounded_size < 10.00
            // strlen("9.99") = 4
            length = 4 + 1 + strlen(units->prefix[i]) + strlen(suffix) + 1;
            formatted_size = malloc(length);
            if(unlikely(formatted_size == NULL)) {
                malloc_error(length);
                return NULL;
            }
            snprintf(formatted_size, length, "%llu%s%02llu %s%s",
                     rounded_size / 100, locale->decimal_point,
                     rounded_size % 100, units->prefix[i], suffix);
        } else { // rounded_size >= 10.00
            rounded_size = (size * 10 + carry_10 + 512) >> 10;
            if(likely(rounded_size < 10000)) { // rounded_size < 1,000.0
                // strlen("999.9") = 5
                length = 5 + 1 + strlen(units->prefix[i]) + strlen(suffix) + 1;
                formatted_size = malloc(length);
                if(unlikely(formatted_size == NULL)) {
                    malloc_error(length);
                    return NULL;
                }
                snprintf(formatted_size, length, "%llu%s%01llu %s%s",
                         rounded_size / 10, locale->decimal_point,
                         rounded_size % 10, units->prefix[i], suffix);
            } else { // rounded_size >= 1,000.0
                rounded_size = (size + 512) >> 10;
                // strlen("1023") = 4
                length = 4 + 1 + strlen(units->prefix[i]) + strlen(suffix) + 1;
                formatted_size = malloc(length);
                if(unlikely(formatted_size == NULL)) {
                    malloc_error(length);
                    return NULL;
                }
                snprintf(formatted_size, length, "%llu %s%s", rounded_size,
                         units->prefix[i], suffix);
            }
        }
    }

    return formatted_size;
}

static int format_n_base10(const prefix_t *units, char *dest, size_t n,
                           kibi_filesize_t size, const char *suffix) {
    kibi_filesize_t rounded_size;
    int i;
    int length;

    if(unlikely(size < 1000)) {
        length = snprintf(dest, n, "%llu %s", size, suffix);
    } else { // size >= 1000
        i = 1;
        // 999950 will be rounded to 1,000,000
        while(size >= 999950) {
            i++;
            size /= 1000;
        }
        if(size < 9995) {
            rounded_size = (size + 5) / 10;
            length = snprintf(dest, n, "%llu%s%02llu %s%s", rounded_size / 100,
                              locale->decimal_point, rounded_size % 100,
                              units->prefix[i], suffix);
        } else { // size >= 9995 (which will be rounded to 10,000)
            rounded_size = (size + 50) / 100;
            length = snprintf(dest, n, "%llu%s%01llu %s%s", rounded_size / 10,
                              locale->decimal_point, rounded_size % 10,
                              units->prefix[i], suffix);
        }
    }

    return length;
}

static int format_n_base2(const prefix_t *units, char *dest, size_t n,
                          kibi_filesize_t size, const char *suffix) {
    kibi_filesize_t carry_10 = 0;
    kibi_filesize_t carry_100 = 0;
    kibi_filesize_t rounded_size;
    int i;
    int length;

    if(size < 1024) {
        length = snprintf(dest, n, "%llu %s", size, suffix);
    } else {
        i = 1;
        // 1023.5 * 1024 = 1,048,064 will be rounded to 1024 * 1024 = 2 ^ 20.
        while(size >= 1048064) {
            i++;
            carry_10 = (carry_10 + 10 * (size & 0x3FF)) >> 10;
            carry_100 = (carry_100 + 100 * (size & 0x3FF)) >> 10;
            size >>= 10;
        }

        rounded_size = (size * 100 + carry_100 + 512) >> 10;
        if(rounded_size < 1000) { // rounded_size < 10.00
            length = snprintf(dest, n, "%llu%s%02llu %s%s", rounded_size / 100,
                              locale->decimal_point, rounded_size % 100,
                              units->prefix[i], suffix);
        } else { // rounded_size >= 10.00
            rounded_size = (size * 10 + carry_10 + 512) >> 10;
            if(likely(rounded_size < 10000)) { // rounded_size < 1,000.0
                length = snprintf(dest, n, "%llu%s%01llu %s%s",
                                  rounded_size / 10, locale->decimal_point,
                                  rounded_size % 10, units->prefix[i], suffix);
            } else { // rounded_size >= 1,000.0
                rounded_size = (size + 512) >> 10;
                length = snprintf(dest, n, "%llu %s%s", rounded_size,
                                  units->prefix[i], suffix);
            }
        }
    }

    return length;
}

static double get_factor_base10(unsigned int level) {
    kibi_filesize_t factor = 1;
    unsigned int i;

    for(i = 0; i < level; i++) {
        factor *= 1000;
    }

    return (double)factor;
}

static double get_factor_base2(unsigned int level) {
    return (double)(1ULL << (10 * level));
}

static const prefix_t base10 = {
    format_size_base10,
    format_n_base10,
    get_factor_base10,
    {"", "k", "M", "G", "T", "P", "E"},
    {"B", "kB", "MB", "GB", "TB", "PB", "EB"},
    {"B/s", "kB/s", "MB/s", "GB/s", "TB/s", "PB/s", "EB/s"},
};

static const prefix_t base2 = {
    format_size_base2,
    format_n_base2,
    get_factor_base2,
    {"", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei"},
    {"B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB"},
    {"B/s", "KiB/s", "MiB/s", "GiB/s", "TiB/s", "PiB/s", "EiB/s"},
};

static const prefix_t historic = {
    format_size_base2,
    format_n_base2,
    get_factor_base2,
    {"", "K", "M", "G", "T", "P", "E"},
    {"B", "KB", "MB", "GB", "TB", "PB", "EB"},
    {"B/s", "KB/s", "MB/s", "GB/s", "TB/s", "PB/s", "EB/s"},
};

static char *get_xdg_config_home(void) {
    const char *home;
    char *result;
    const char *config_dir = ".config";
    const char *xdg_config_home;
    size_t length;

    xdg_config_home = getenv("XDG_CONFIG_HOME");
    if(xdg_config_home != NULL && strlen(xdg_config_home) > 0) {
        length = strlen(xdg_config_home) + 1;
        result = malloc(length);
        if(unlikely(result == NULL)) {
            malloc_error(length);
            return NULL;
        }
        strncpy(result, xdg_config_home, length);
    } else { // xdg_config_home == NULL || strlen(xdg_config_home) == 0
        // Use $HOME/.config if XDG_CONFIG_HOME is not defined.
        home = getenv("HOME");
        length = strlen(home) + 1 + strlen(config_dir) + 1;
        result = malloc(length);
        if(unlikely(result == NULL)) {
            malloc_error(length);
            return NULL;
        }
        snprintf(result, length, "%s/%s", home, config_dir);
    }
    return result;
}

static char *get_user_config_file(void) {
    char *xdg_config_home = get_xdg_config_home();
    const char *config_file = "byteprefix";
    char *result;
    size_t length;

    if(xdg_config_home == NULL) {
        return NULL;
    }
    length = strlen(xdg_config_home) + 1 + strlen(config_file) + 1;
    result = malloc(length);
    if(unlikely(result == NULL)) {
        malloc_error(length);
        free(xdg_config_home);
        return NULL;
    }
    snprintf(result, length, "%s/%s", xdg_config_home, config_file);
    free(xdg_config_home);
    return result;
}

static char *strip(char *str) {
    int i;

    while(*str == ' ' || *str == '\n' || *str == '\r') {
        str++;
    }
    for(i = strlen(str) - 1; i >= 0; i--) {
        if(str[i] != ' ' && str[i] != '\n' && str[i] != '\r') {
            break;
        }
        str[i] = '\0';
    }
    return str;
}

static bool analyze_config_value(const char *value) {
    bool found_format = false;

    if(strcmp("base2", value) == 0) {
        selected_prefix = &base2;
        found_format = true;
    } else if(strcmp("base10", value) == 0) {
        selected_prefix = &base10;
        found_format = true;
    } else if(strcmp("historic", value) == 0) {
        selected_prefix = &historic;
        found_format = true;
    }

    return found_format;
}

static bool read_config_file(const char *user_config_file) {
    FILE *stream;
    char buffer[MAX_LEN + 1];
    char *format;
    bool found_format = false;

    stream = fopen(user_config_file, "r");
    if(stream != NULL) {
        while(fgets(buffer, MAX_LEN + 1, stream) != NULL) {
            if(!strncmp("format=", buffer, strlen("format="))) {
                format = &buffer[strlen("format=")];
                format = strip(format);
                debug("Found format value \"%s\"\n", format);
                if(analyze_config_value(format)) {
                    found_format = true;
                }
            }
        }
    }

    return found_format;
}

static void kibi_init(void) {
    char *byteprefix_env;
    char *user_config_file = get_user_config_file();
    bool found_config = false;

    byteprefix_env = getenv("BYTEPREFIX");
    if(byteprefix_env != NULL) {
        found_config = analyze_config_value(byteprefix_env);
    }

    if(!found_config) {
        if(!access(user_config_file, R_OK)) {
            debug("found %s\n", user_config_file);
            found_config = read_config_file(user_config_file);
        } else { // access(user_config_file, R_OK)
            if(!access(user_config_file, F_OK)) {
                fprintf(stderr,
                        "\nlibkibi: Warning: Can't read config file %s\n",
                        user_config_file);
            } else { // access(user_config_file, F_OK)
                debug("not found %s\n", user_config_file);
            }
        }
    }
    free(user_config_file);

    if(!found_config) {
        if(!access(SYSTEM_CONFIG_FILE, R_OK)) {
            debug("found %s\n", SYSTEM_CONFIG_FILE);
            found_config = read_config_file(SYSTEM_CONFIG_FILE);
        } else { // access(SYSTEM_CONFIG_FILE, R_OK)
            if(!access(SYSTEM_CONFIG_FILE, F_OK)) {
                fprintf(stderr,
                        "\nlibkibi: Warning: Can't read config file %s\n",
                        SYSTEM_CONFIG_FILE);
            } else { // access(SYSTEM_CONFIG_FILE, F_OK)
                debug("not found %s\n", SYSTEM_CONFIG_FILE);
            }
        }
    }

    if(selected_prefix == NULL) {
        // No user or system configuration
        selected_prefix = &DEFAULT_PREFIX;
    }

    locale = localeconv();
}

double kibi_divide_size(kibi_filesize_t size, unsigned int level) {
    if(unlikely(selected_prefix == NULL)) {
        kibi_init();
    }

    if(unlikely(level >= PREFIX_COUNT)) {
        level = PREFIX_COUNT - 1;
    }

    return (double)size / selected_prefix->get_factor(level);
}

char *kibi_format_memory_size(kibi_filesize_t memory_size) {
    char *formatted_size;

    if(unlikely(selected_prefix == NULL)) {
        kibi_init();
    }

    if(selected_prefix == &historic) {
        formatted_size = historic.format_size(&historic, memory_size, "B");
    } else { // selected_prefix != &historic
        formatted_size = base2.format_size(&base2, memory_size, "B");
    }

    return formatted_size;
}

char *kibi_format_si_unit(kibi_filesize_t value, const char *unit) {
    if(unlikely(unit == NULL)) {
        unit = "(null)";
    }

    return base10.format_size(&base10, value, unit);
}

char *kibi_format_size(kibi_filesize_t size) {
    if(unlikely(selected_prefix == NULL)) {
        kibi_init();
    }

    return selected_prefix->format_size(selected_prefix, size, "B");
}

char *kibi_format_size_detailed(kibi_filesize_t size) {
    char *base2_size;
    char *base10_size;
    char *formatted_size;
    size_t length;

    if(unlikely(selected_prefix == NULL)) {
        kibi_init();
    }

    if(size >= 1024 && selected_prefix != &historic) {
        base2_size = base2.format_size(&base2, size, "B");
        base10_size = base10.format_size(&base10, size, "B");
        length = strlen(base10_size) + 1 + strlen(base2_size) + 1;
        formatted_size = malloc(length);
        if(unlikely(base2_size == NULL || base10_size == NULL ||
                    formatted_size == NULL)) {
            malloc_error(length);
            free(base2_size);
            free(base10_size);
            return NULL;
        }
        if(selected_prefix == &base10) {
            snprintf(formatted_size, length, "%s/%s", base10_size, base2_size);
        } else { // selected_prefix == &base2
            snprintf(formatted_size, length, "%s/%s", base2_size, base10_size);
        }
        free(base2_size);
        free(base10_size);
    } else { // size < 1024 || selected_prefix == &historic
        formatted_size = selected_prefix->format_size(selected_prefix, size,
                                                      "B");
    }

    return formatted_size;
}

char *kibi_format_transfer_rate(kibi_filesize_t transfer_rate) {
    if(unlikely(selected_prefix == NULL)) {
        kibi_init();
    }

    return selected_prefix->format_size(selected_prefix, transfer_rate, "B/s");
}

char *kibi_format_value(kibi_filesize_t value, const char *suffix) {
    if(unlikely(selected_prefix == NULL)) {
        kibi_init();
    }

    if(unlikely(suffix == NULL)) {
        suffix = "(null)";
    }

    return selected_prefix->format_size(selected_prefix, value, suffix);
}

const char *kibi_get_unit_for_size(unsigned int level) {
    if(unlikely(selected_prefix == NULL)) {
        kibi_init();
    }

    if(unlikely(level >= PREFIX_COUNT)) {
        level = PREFIX_COUNT - 1;
    }
    return selected_prefix->size[level];
}

const char *kibi_get_unit_for_transfer_rate(unsigned int level) {
    if(unlikely(selected_prefix == NULL)) {
        kibi_init();
    }

    if(unlikely(level >= PREFIX_COUNT)) {
        level = PREFIX_COUNT - 1;
    }
    return selected_prefix->transfer_rate[level];
}

kibi_filesize_t kibi_multiply_size(double size, unsigned int level) {
    if(unlikely(selected_prefix == NULL)) {
        kibi_init();
    }

    if(unlikely(level >= PREFIX_COUNT)) {
        level = PREFIX_COUNT - 1;
    }

    return (kibi_filesize_t)(size * selected_prefix->get_factor(level) + 0.5);
}

int kibi_n_format_memory_size(char *dest, size_t n,
                              kibi_filesize_t memory_size) {
    int length;

    if(unlikely(selected_prefix == NULL)) {
        kibi_init();
    }

    if(selected_prefix == &historic) {
        length = historic.format_n(&historic, dest, n, memory_size, "B");
    } else { // selected_prefix != &historic
        length = base2.format_n(&base2, dest, n, memory_size, "B");
    }

    return length;
}

int kibi_n_format_si_unit(char *dest, size_t n, kibi_filesize_t value,
                          const char *unit) {
    return base10.format_n(&base10, dest, n, value, unit);
}

int kibi_n_format_size(char *dest, size_t n, kibi_filesize_t size) {
    if(unlikely(selected_prefix == NULL)) {
        kibi_init();
    }

    return selected_prefix->format_n(selected_prefix, dest, n, size, "B");
}

int kibi_n_format_size_detailed(char *dest, size_t n, kibi_filesize_t size) {
    char base2_size[BASE2_SIZE_LENGTH];
    char base10_size[BASE10_SIZE_LENGTH];
    int length;

    if(unlikely(selected_prefix == NULL)) {
        kibi_init();
    }

    if(size >= 1024 && selected_prefix != &historic) {
        base2.format_n(&base2, base2_size, BASE2_SIZE_LENGTH, size, "B");
        base10.format_n(&base10, base10_size, BASE10_SIZE_LENGTH, size, "B");
        if(selected_prefix == &base10) {
            length = snprintf(dest, n, "%s/%s", base10_size, base2_size);
        } else { // selected_prefix == &base2
            length = snprintf(dest, n, "%s/%s", base2_size, base10_size);
        }
    } else { // size < 1024 || selected_prefix == &historic
        length = selected_prefix->format_n(selected_prefix, dest, n, size, "B");
    }

    return length;
}

int kibi_n_format_transfer_rate(char *dest, size_t n,
                                kibi_filesize_t transfer_rate) {
    if(unlikely(selected_prefix == NULL)) {
        kibi_init();
    }

    return selected_prefix->format_n(selected_prefix, dest, n, transfer_rate,
                                     "B/s");
}

int kibi_n_format_value(char *dest, size_t n, kibi_filesize_t value,
                        const char *suffix) {
    if(unlikely(selected_prefix == NULL)) {
        kibi_init();
    }

    return selected_prefix->format_n(selected_prefix, dest, n, value, suffix);
}
