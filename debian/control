Source: libkibi
Section: libs
Priority: optional
Maintainer: Benjamin Drung <bdrung@debian.org>
Build-Depends: debhelper (>= 10~)
Standards-Version: 3.9.8
Homepage: https://launchpad.net/libkibi
Vcs-Git: https://salsa.debian.org/debian/libkibi.git
Vcs-Browser: https://salsa.debian.org/debian/libkibi

Package: libkibi0
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: library for byte prefixes
 This library is designed for formatting sizes in bytes for display. The user
 can configure a preferred prefix style.
 .
 Three different types of byte prefixes can be selected:
  * kB, MB, GB with base 1000 (base10)
  * KiB, MiB, GiB with base 1024 (base2)
  * KB, MB, GB with base 1024 (historic)

Package: libkibi-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libkibi0 (= ${binary:Version}), ${misc:Depends}
Description: library for byte prefixes (development files)
 This library is designed for formatting sizes in bytes for display. The user
 can configure a preferred prefix style.
 .
 Three different types of byte prefixes can be selected:
  * kB, MB, GB with base 1000 (base10)
  * KiB, MiB, GiB with base 1024 (base2)
  * KB, MB, GB with base 1024 (historic)
 .
 This package contains the development files.
