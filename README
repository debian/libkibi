General Information
===================

libkibi is a library for byte prefixes. It's designed for formatting sizes in
bytes for display. The user can configure a preferred prefix style.

libkibi is free software and licensed under the ISC license. See COPYING for
details.

Installation
============

libkibi can be configured, build, tested, and installed with following three
commands:

    ./configure
    make check
    make install

The configure command takes a --with-default-prefix parameter that sets the
compile time default byte prefix. This default will be used if the user doesn't
set up a byteprefix configuration file or environment variable.

User configuration
==================

The user configuration is described in the byteprefix man page. It can be read
after the installation of libkibi with

    man 5 byteprefix

Information for developers
==========================

The first step is to include the header from libkibi:

    #include <kibi.h>

All public libkibi functions start with kibi_. The functions are described in
the kibi.h header file in detail.

Output functions
----------------

These functions can be used to format sizes for output. All these functions
allocate memory for storing the resulting string. You have to call free() to
release the memory after usage.

    kibi_format_size()
    kibi_format_size_detailed()
    kibi_format_transfer_rate()
    kibi_format_memory_size()
    kibi_format_si_unit()

libkibi has corresponding functions that take a character buffer as input
instead of allocating memory:

    kibi_n_format_size()
    kibi_n_format_size_detailed()
    kibi_n_format_transfer_rate()
    kibi_n_format_memory_size()
    kibi_n_format_si_unit()

To format custom units, there are kibi_format_value() and kibi_n_format_value().
Use them with care.

Input functions
---------------

These functions can be used for converting between sizes with units (for example
to allow a user to edit a value in kB/s):

    kibi_divide_size()
    kibi_get_unit_for_size()
    kibi_get_unit_for_transfer_rate()
    kibi_multiply_size()

Asking questions
================

Questions can be asked on Launchpad:

    https://answers.launchpad.net/libkibi

Reporting bugs
==============

Bugs should be reported on Launchpad:

    https://bugs.launchpad.net/libkibi

Developing libkibi
==================

bzr is used a version control system. You can check out the latest development
branch with

    bzr branch lp:libkibi

After the checkout, autoreconf needs to be run:

    autoreconf -i

Submitting improvement
======================

You can either attach patches in unified diff form (The -up option to GNUdiff.)
to bug reports or open merge proposals to the lp:libkibi branch.
